# OMO Semestálna práca - Smart factory

## Členovia týmu
Jaroslav Jelínek

## Popis semestrálnej práce 
Rozhodol som sa spracovať tému Smart Factory. Továreň pozostáva z mnoha entít. Jednotlivé produkty (vytvorený produkt je instancia triedy Item), vyrábané z rôznych materiálov na produkčných linkách, sú vyrábané pracovníkmi, robotmi a strojmi. Tie si budú prechádzať stavmi. Robot a stroj sa môžu pokaziť, pričom ich opraví opravár. Pri pokazení alebo inej udalosti sa vygeneruje event, ktorý sa uloží do EventPoolu na jeho následné spracovanie. Počas náhodného ticku môžu prísť obhliadnuť továreň riaditeľ a inšpektor, ktorý prehliadaju entity (riaditeľ aj produkty pri výrobných linkách) a zapisujú ich stav do logu. Semestrálna práca tiež obsahuje report creatory, ktoré generujú reporty z rôznych udalostí za nejaké časové obdobie. Na záver sa počiatočný stav továrne konfiguruje cez externý JSON súbor kvôli prehľadnosti.

## Design patterny semestrálnej práce
 **State Machine** - FactoryEntity,
 **Iterator** - InspectorIterator a DirectorIterator,
 **Factory method** -  Report a ReportCreator,
 **Singleton** - Factory, FactoryConfig, Storage a EventPool,
 **Visitor** - Inspector a Director, 
 **Observer, observable** - FactoryEntity a EventPool,
 **Listener** - Repairman,
 **Partially persistent data structure** - FactoryEntity,
 **Object pool** - EventPool,
 **Lazy loading** - Inspector a Director

## Dojmy zo semestrálnej práce
Napriek náročnosti semestrálnej práce hlavne z dôvodu samostatnej práce som spokojný s výsledkom a kompletnosťou programu. S veľa vecami som sa pri práci stretol po prvýkrát, preto som často si o daných veciach zisťoval viac, poprípade mi boli vysvetlené. Som rád, že predmet OMO má za semestrálnu prácu práve niečo väčšieho rozsahu, kde môžme na konci vidieť, ako jednotlivé komponenty spolu vytvoria jeden celok