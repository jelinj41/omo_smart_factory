package cz.cvut.k36.omo.smartfactory.production;

import java.util.List;
import java.util.Objects;

/**
 * A product produced in a factory
 */
public class Product {

    private String name;
    private int amountInSeries;
    private List<Material> materialsNeeded;
    private List<String> productionLineConfig;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmountInSeries() {
        return amountInSeries;
    }

    public void setAmountInSeries(int amountInSeries) {
        this.amountInSeries = amountInSeries;
    }

    public List<String> getProductionLineConfig() {
        return productionLineConfig;
    }

    public void setProductionLineConfig(List<String> productionLineConfig) {
        this.productionLineConfig = productionLineConfig;
    }

    public List<Material> getMaterialsNeeded() {
        return materialsNeeded;
    }

    public void setMaterialsNeeded(List<Material> materialsNeeded) {
        this.materialsNeeded = materialsNeeded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(getName(), product.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        return "Product (" + name + ")";
    }
}
