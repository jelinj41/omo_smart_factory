package cz.cvut.k36.omo.smartfactory.production;

import cz.cvut.k36.omo.smartfactory.entities.Entity;
import cz.cvut.k36.omo.smartfactory.inspection.Inspectable;
import cz.cvut.k36.omo.smartfactory.inspection.InspectingPerson;

import java.util.List;

/**
 * An item manufactured in a factory
 */
public class Item implements Inspectable {

    private String itemID;
    private Product product;
    private List<Entity> remainingWorkers;
    private int finishedInTick;

    public Item(String id, Product product, List<Entity> remainingWorkers) {
        this.itemID = id;
        this.product = product;
        this.remainingWorkers = remainingWorkers;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Entity> getRemainingWorkers() {
        return remainingWorkers;
    }

    public void setRemainingWorkers(List<Entity> remainingWorkers) {
        this.remainingWorkers = remainingWorkers;
    }

    public int getFinishedInTick() {
        return finishedInTick;
    }

    public void setFinishedInTick(int finishedInTick) {
        this.finishedInTick = finishedInTick;
    }

    @Override
    public String toString() {
        return "Item (ID " + itemID + ", " + product + ")";
    }

    @Override
    public void accept(InspectingPerson inspectingPerson) {
        inspectingPerson.visitItem(this);
    }

}
