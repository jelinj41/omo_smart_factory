package cz.cvut.k36.omo.smartfactory.entities.state;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.entities.Worker;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;

/**
 * Free state used on all entities, meaning that an entity is free and can work on something else
 */
public class Free implements State {

    @Override
    public void workerState(Worker context, ProductionLine line, Item item) {
        System.out.println(context + " is free and has nothing to work on.");
    }

    @Override
    public void robotState(Robot context, ProductionLine line, Item item) {
        System.out.println(context + " is free and has nothing to work on.");
    }

    @Override
    public void machineState(Machine context, ProductionLine line, Item item) {
        System.out.println(context + " is free and has nothing to work on.");
    }

}
