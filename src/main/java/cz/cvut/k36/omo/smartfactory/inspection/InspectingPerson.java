package cz.cvut.k36.omo.smartfactory.inspection;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.entities.Worker;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;

/**
 * An interface with methods for both an inspector and a director
 */
public interface InspectingPerson {

    void visitWorker(Worker worker);
    void visitRobot(Robot robot);
    void visitMachine(Machine machine);
    void visitItem(Item item);

}
