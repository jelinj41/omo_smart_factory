package cz.cvut.k36.omo.smartfactory.production;

import cz.cvut.k36.omo.smartfactory.Main;
import cz.cvut.k36.omo.smartfactory.entities.Repairman;
import cz.cvut.k36.omo.smartfactory.event.EventPool;
import cz.cvut.k36.omo.smartfactory.configuration.FactoryConfig;
import cz.cvut.k36.omo.smartfactory.configuration.InvalidConfigValueException;
import cz.cvut.k36.omo.smartfactory.inspection.DirectorIterator;
import cz.cvut.k36.omo.smartfactory.inspection.InspectorIterator;
import cz.cvut.k36.omo.smartfactory.inspection.Director;
import cz.cvut.k36.omo.smartfactory.inspection.Inspector;
import cz.cvut.k36.omo.smartfactory.utils.RNG;

import java.util.ArrayList;
import java.util.List;

/**
 * A factory where all operations take place
 */
public class Factory {

    private static Factory instanceOf = null;
    private int tick = 0;
    private Product currentProductSeries;
    private EventPool eventPool;
    private FactoryConfig factoryConfig;

    private List<ProductionLine> productionLines;
    private List<Repairman> repairmen;
    private List<Product> products;
    private Storage storage;

    private Director director;
    private Inspector inspector;


    private Factory() throws InvalidConfigValueException {
        factoryConfig = FactoryConfig.getInstanceOf();
        eventPool = EventPool.getInstanceOf();

        productionLines = new ArrayList<>();

        for (int i = 1; i <= factoryConfig.getNumOfProductionLines(); i++) {
            productionLines.add(new ProductionLine(i, factoryConfig.getNumOfProductionLines() - i));
        }

        repairmen = new ArrayList<>();

        for (int i = 1; i <= factoryConfig.getNumOfRepairmen(); i++) {
            repairmen.add(new Repairman(i, RNG.getInstanceOf().randomInt(10, 20)));
        }

        products = new ArrayList<>(factoryConfig.getProducts());
        storage = Storage.getInstanceOf();

        createEntities();
    }

    public static Factory getInstanceOf() {
        if (instanceOf == null) {
            try {
                instanceOf = new Factory();
            } catch (InvalidConfigValueException e2) {
                System.err.println("Invalid value in factory configuration file number (" + Main.getConfigFileId() + "): " + e2.getMessage());
                System.exit(1);
            }
        }

        return instanceOf;
    }

    public int getTick() {
        return tick;
    }

    public EventPool getEventPool() {
        return eventPool;
    }

    public FactoryConfig getFactoryConfig() {
        return factoryConfig;
    }

    public List<ProductionLine> getProductionLines() {
        return productionLines;
    }

    public Storage getStorage() {
        return storage;
    }

    public void putItemToStorage(Item item) {
        storage.addIntoStorage(item);
    }

    private DirectorIterator createDirectorIterator() {
        return new DirectorIterator(this);
    }

    private InspectorIterator createInspectorIterator() {
        return new InspectorIterator(this);
    }

    public static void clearFactory() {
        instanceOf = null;
    }

    public void run() {
        for (Product product : products) {
            if (!product.equals(currentProductSeries)) {
                configureProductionLines(product);
            }

            while (!allLinesFinished()) {
                nextTick();
            }
        }

        System.out.println("\nWork is done! All product series have been manufactured.\n");
        System.out.println(storage);
    }

    private void createEntities() {
        for (ProductionLine line : productionLines) {
            line.createEntities(products);
        }
    }

    public void nextTick() {
        System.out.println("\nTick " + tick + "\n");
        runLines();
        repairmenWork();

        if (RNG.getInstanceOf().calculateProbability(0.01)) {
            inspectorWork();
        }

        if (RNG.getInstanceOf().calculateProbability(0.01)) {
            directorWork();
        }

        tick++;
    }

    public void configureProductionLines(Product product) {
        System.out.println("\nTick " + tick + "\n");
        System.out.println("Configuring production lines...");
        currentProductSeries = product;
        int itemsForOneLine = product.getAmountInSeries() / productionLines.size();

        for (int i = 0; i < productionLines.size(); i++) {
            ProductionLine line = productionLines.get(i);

            if (i == productionLines.size() - 1) {
                itemsForOneLine += product.getAmountInSeries() % productionLines.size();
            }

            line.configurePL(product, itemsForOneLine);
            System.out.println("Configured " + line);
        }
        tick++;
    }

    private void runLines() {
        for (ProductionLine line : productionLines) {
            line.run();
        }
    }

    private boolean allLinesFinished() {
        return productionLines.stream().allMatch(ProductionLine::isFinished);
    }

    private void repairmenWork() {
        for (Repairman repairman : repairmen) {
            repairman.workOn(null, null);
        }
    }

    private void inspectorWork() {
        if (inspector == null) {
            inspector = new Inspector();
        }

        inspector.iLogger.info("\nInspection " + inspector.getNumOfInspections() + "\n");
        inspector.addInspection();
        InspectorIterator inspectorIterator = createInspectorIterator();

        while (inspectorIterator.hasNext()) {
            inspectorIterator.nextEntity().accept(inspector);
        }
    }

    private void directorWork() {
        if (director == null) {
            director = new Director();
        }

        director.dLogger.info("\nInspection " + director.getNumOfInspections() + "\n");
        director.addInspection();

        DirectorIterator directorIterator = createDirectorIterator();

        while (directorIterator.hasNextEntity()) {
            directorIterator.nextEntity().accept(director);
        }

        while (directorIterator.hasNextItem()) {
            Item item = directorIterator.nextItem();
            if (item != null) {
                item.accept(director);
            }
        }
    }

}
