package cz.cvut.k36.omo.smartfactory.information;

import cz.cvut.k36.omo.smartfactory.event.Event;
import cz.cvut.k36.omo.smartfactory.entities.Entity;

/**
 * Info about started repair
 */
public class RepairStartedInfo extends Event {

    private Entity repairedEntity;

    public RepairStartedInfo(Entity creator, int tick, Entity repairedEntity) {
        super(creator, tick);
        this.repairedEntity = repairedEntity;
    }

    public Entity getRepairedEntity() {
        return repairedEntity;
    }

}
