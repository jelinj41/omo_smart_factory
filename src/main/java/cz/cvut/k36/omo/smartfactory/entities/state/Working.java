package cz.cvut.k36.omo.smartfactory.entities.state;

import cz.cvut.k36.omo.smartfactory.production.Factory;
import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.information.EventInfo;
import cz.cvut.k36.omo.smartfactory.information.MalfunctionInfo;
import cz.cvut.k36.omo.smartfactory.entities.Worker;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;
import cz.cvut.k36.omo.smartfactory.utils.Constants;
import cz.cvut.k36.omo.smartfactory.utils.RNG;

import java.util.Arrays;

/**
 * Working state used on all entities, meaning an entity is working on something
 */
public class Working implements State {

    @Override
    public void workerState(Worker context, ProductionLine line, Item item) {
        System.out.println(context + " is working on " + item + " and used " + Arrays.toString(item.getProduct().getMaterialsNeeded().toArray()));
        item.getRemainingWorkers().remove(context);
        context.notifyAllObservers(new EventInfo(context, Factory.getInstanceOf().getTick(), context.getCost(),
                0, 0, item));
    }

    @Override
    public void robotState(Robot context, ProductionLine line, Item item) {
        if (RNG.getInstanceOf().calculateProbability(context.getDamage())) {
            context.setDamage(1);
            context.setState(new Broken());
            System.out.println(context + " broke");
            context.notifyAllObservers(new MalfunctionInfo(context, Factory.getInstanceOf().getTick(), line.getPriority()));
        } else {
            System.out.println(context + " is working on " + item + " and used " + Arrays.toString(item.getProduct().getMaterialsNeeded().toArray()));
            item.getRemainingWorkers().remove(context);
            context.notifyAllObservers(new EventInfo(context, Factory.getInstanceOf().getTick(),
                    context.getCost(), context.getFuel(),
                    context.getElectricity(), item));
            context.setDamage(context.getDamage() + Constants.ROBOT_DAMAGE_ONETICK);
        }
    }

    @Override
    public void machineState(Machine context, ProductionLine line, Item item) {
        if (RNG.getInstanceOf().calculateProbability(context.getDamage())) {
            context.setDamage(1);
            context.setState(new Broken());
            System.out.println(context + " broke!");
            context.notifyAllObservers(new MalfunctionInfo(context, Factory.getInstanceOf().getTick(), line.getPriority()));
        } else {
            System.out.println(context + " is working on " + item + " and used " + Arrays.toString(item.getProduct().getMaterialsNeeded().toArray()));
            item.getRemainingWorkers().remove(context);
            context.notifyAllObservers(new EventInfo(context, Factory.getInstanceOf().getTick(),
                    context.getCost(), context.getFuel(),
                    context.getElectricity(), item));
            context.setDamage(context.getDamage() + Constants.MACHINE_DAMAGE_ONETICK);
        }
    }

}
