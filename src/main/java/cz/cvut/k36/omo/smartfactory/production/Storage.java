package cz.cvut.k36.omo.smartfactory.production;

import java.util.*;

/**
 * A storage for all items where items of the same product are then stored together in a list
 */
public class Storage {
    private Map<Product, List<Item>> items;
    private static Storage instanceOf = null;

    private Storage() {
        items = new HashMap<>();
    }

    public Map<Product, List<Item>> getItems() {
        return items;
    }

    public static Storage getInstanceOf() {
        if (instanceOf == null) {
            instanceOf = new Storage();
        }

        return instanceOf;
    }

    public void addIntoStorage(Item item) {
        List<Item> items = this.items.get(item.getProduct());

        if (items != null) {
            items.add(item);
            items.sort(Comparator.comparing(Item::getFinishedInTick));
        } else {
            this.items.put(item.getProduct(), new ArrayList<>(Collections.singleton(item)));
        }
    }

    public static void clearStorage() {
        instanceOf = null;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Storage:\n");

        for (Product product : items.keySet()) {
            str.append(product).append(" - ").append(items.get(product).size()).append(" items\n");
        }

        return str.toString();
    }
}
