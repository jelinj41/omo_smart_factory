package cz.cvut.k36.omo.smartfactory.entities;

import cz.cvut.k36.omo.smartfactory.production.Factory;
import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.event.*;
import cz.cvut.k36.omo.smartfactory.information.MalfunctionInfo;
import cz.cvut.k36.omo.smartfactory.information.RepairEndedInfo;
import cz.cvut.k36.omo.smartfactory.information.RepairStartedInfo;
import cz.cvut.k36.omo.smartfactory.entities.state.Free;
import cz.cvut.k36.omo.smartfactory.entities.state.Working;
import cz.cvut.k36.omo.smartfactory.utils.RNG;

/**
 * A repairman who fixes machines and robots
 */
public class Repairman extends Worker implements Listener {

    private int repairmanID;
    private int remainingTicks;
    private Entity isWorkingOn;

    public Repairman(int repairmanID, int cost) {
        super(cost);
        this.repairmanID = repairmanID;
    }

    @Override
    public void workOn(ProductionLine line, Item item) {
        if (isWorking()) {
            remainingTicks--;

            if (remainingTicks == 0) {
                repair();
                notifyAllObservers(new RepairEndedInfo(this, Factory.getInstanceOf().getTick(), isWorkingOn));
                isWorkingOn = null;
            }
        } else {
            listen();
        }
    }

    @Override
    public void listen() {
        Event event = EventPool.getInstanceOf().getEvent(this, MalfunctionInfo.class);

        if (event != null) {
            MalfunctionInfo malfunctionInfo = (MalfunctionInfo) event;
            isWorkingOn = malfunctionInfo.getCreator();
            remainingTicks = RNG.getInstanceOf().randomInt(2, 5);
            setState(new Working());
            notifyAllObservers(new RepairStartedInfo(this, Factory.getInstanceOf().getTick(), isWorkingOn));
        }
    }

    private void repair() {
        isWorkingOn.setState(new Free());
        setState(new Free());

        if (isWorkingOn instanceof Robot) {
            ((Robot) isWorkingOn).setDamage(0);
        }

        if (isWorkingOn instanceof Machine) {
            ((Machine) isWorkingOn).setDamage(0);
        }

        System.out.println(isWorkingOn + " is repaired!");
    }

    @Override
    public String toString() {
        return "Repairman (ID " + repairmanID + ")";
    }
}
