package cz.cvut.k36.omo.smartfactory.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.k36.omo.smartfactory.Main;
import cz.cvut.k36.omo.smartfactory.production.Product;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Initial configuration of the factory
 */
public class FactoryConfig {

    private static FactoryConfig instanceOf = null;
    private Integer numOfProductionLines;
    private List<Product> products;
    private Integer numOfRepairmen;
    private String randomGeneratorSeed;


    public static FactoryConfig getInstanceOf() {
        if (instanceOf == null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                instanceOf = objectMapper.readValue(
                        new File("config" + Main.getConfigFileId() + ".json"), FactoryConfig.class
                );
            } catch (IOException e1) {
                e1.printStackTrace();
                System.err.println("Factory configuration file with id (" + Main.getConfigFileId() + ") is missing or corrupt.");
                System.exit(1);
            }
        }

        return instanceOf;
    }

    public static void clearFactoryConfig() {
        instanceOf = null;
    }

    public Integer getNumOfProductionLines() throws InvalidConfigValueException {
        if (numOfProductionLines >= 0) {
            return numOfProductionLines;
        } else {
            throw new InvalidConfigValueException("Invalid value of production lines: " + numOfProductionLines);
        }
    }

    public void setNumOfProductionLines(Integer numOfProductionLines) throws InvalidConfigValueException {
        if (numOfProductionLines >= 0) {
            this.numOfProductionLines = numOfProductionLines;
        } else {
            throw new InvalidConfigValueException("Invalid value of production lines: " + numOfProductionLines);
        }
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Integer getNumOfRepairmen() throws InvalidConfigValueException {
        if (numOfRepairmen >= 0) {
            return numOfRepairmen;
        } else {
            throw new InvalidConfigValueException("Invalid value of repairmen: " + numOfRepairmen);
        }
    }

    public void setNumOfRepairmen(Integer numOfRepairmen) throws InvalidConfigValueException {
        if (numOfRepairmen >= 0) {
            this.numOfRepairmen = numOfRepairmen;
        } else {
            throw new InvalidConfigValueException("Invalid value of repairmen: " + numOfRepairmen);
        }
    }

    public String getRandomGeneratorSeed() {
        return randomGeneratorSeed;
    }

    public void setRandomGeneratorSeed(String randomGeneratorSeed) {
        this.randomGeneratorSeed = randomGeneratorSeed;
    }

}
