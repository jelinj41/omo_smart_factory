package cz.cvut.k36.omo.smartfactory.event;

import cz.cvut.k36.omo.smartfactory.entities.Entity;

import java.util.Objects;

/**
 * Abstract class - An event happening on factory entities in a factory.
 */
public abstract class Event {

    private Entity creator;
    private int tick;
    private Listener processedBy;

    public Event(Entity creator, int tick) {
        this.creator = creator;
        this.tick = tick;
        processedBy = null;
    }

    public Entity getCreator() {
        return creator;
    }

    public void setCreator(Entity creator) {
        this.creator = creator;
    }

    public int getTick() {
        return tick;
    }

    public void setTick(int tick) {
        this.tick = tick;
    }

    public boolean isProcessed() {
        return processedBy != null;
    }

    public Listener getProcessedBy() {
        return processedBy;
    }

    public void setProcessedBy(Listener processedBy) {
        this.processedBy = processedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return getTick() == event.getTick() &&
                getCreator().equals(event.getCreator());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCreator(), getTick());
    }

    @Override
    public String toString() {
        return "Event (" + getClass().getSimpleName() + ", " + creator + ", tick=" + tick + ")";
    }
}
