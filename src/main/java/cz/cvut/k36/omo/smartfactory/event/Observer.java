package cz.cvut.k36.omo.smartfactory.event;


public interface Observer {

    public void update(Event event);

}
