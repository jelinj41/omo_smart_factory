package cz.cvut.k36.omo.smartfactory.entities.state;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.entities.Worker;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;

/**
 * Broken state, that is only used on robots and machines
 */
public class Broken implements State {

    @Override
    public void workerState(Worker context, ProductionLine line, Item item) {}

    @Override
    public void robotState(Robot context, ProductionLine line, Item item) {
        System.err.println(context + " is broken and needs fixing.");
    }

    @Override
    public void machineState(Machine context, ProductionLine line, Item item) {
        System.err.println(context + " is broken and needs fixing.");
    }

}
