package cz.cvut.k36.omo.smartfactory.inspection;

import cz.cvut.k36.omo.smartfactory.production.Factory;
import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.entities.Entity;

import java.util.Iterator;

/**
 * An iterator for director - inspection of all entities according to the hierarchy
 */
public class DirectorIterator {

    private int currLine;
    private Factory factory;
    private Iterator<Entity> entityIterator;
    private Iterator<Item> itemIterator;

    public DirectorIterator(Factory factory) {
        currLine = 0;
        this.factory = factory;
        ProductionLine currentLine = factory.getProductionLines().get(currLine);
        entityIterator = currentLine.getConfig().iterator();
        itemIterator = currentLine.getItemQueue().iterator();
    }


    public boolean hasNextEntity() {
        boolean lineHasNextEntity = entityIterator.hasNext();

        if (! lineHasNextEntity) {
            if (currLine < factory.getProductionLines().size() - 1) {
                currLine++;
                ProductionLine currentLine = factory.getProductionLines().get(currLine);
                entityIterator = currentLine.getConfig().iterator();
                return hasNextEntity();
            }

            return false;
        }

        return true;
    }

    public Entity nextEntity() {
        return entityIterator.next();
    }


    public boolean hasNextItem() {
        boolean lineHasNextItem = itemIterator.hasNext();

        if (! lineHasNextItem) {
            if (currLine < factory.getProductionLines().size() - 1) {
                currLine++;
                ProductionLine currentLine = factory.getProductionLines().get(currLine);
                itemIterator = currentLine.getItemQueue().iterator();
                return hasNextItem();
            }

            return false;
        }

        return true;
    }

    public Item nextItem() {
        return itemIterator.next();
    }
}
