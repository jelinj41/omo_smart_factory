package cz.cvut.k36.omo.smartfactory.inspection;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.entities.Worker;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;
import cz.cvut.k36.omo.smartfactory.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * The inspector visits machines and robots and "writes down"
 * their state and damage.
 */
public class Inspector implements InspectingPerson {

    private int numOfInspections = 1;
    public final Logger iLogger;
    private FileHandler fileHandler;

    public Inspector() {
        iLogger = Logger.getLogger(Inspector.class.getName());
        iLogger.setLevel(Level.INFO);
        makeNonExistingLogsDirectory();

        try {
            fileHandler = new FileHandler(Constants.LOGS_DIRECTORY_NAME + "/inspector.txt");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        fileHandler.setFormatter(new SimpleFormatter());
        iLogger.addHandler(fileHandler);
    }

    private void makeNonExistingLogsDirectory() {
        File logsDirectory = new File(Constants.LOGS_DIRECTORY_NAME);
        if (!logsDirectory.exists()) {
            boolean created = logsDirectory.mkdir();
            if (!created) {
                throw new RuntimeException("Logs directory is missing and could not be created.");
            }
        }
    }

    public void addInspection() {
        numOfInspections++;
    }

    public int getNumOfInspections() {
        return numOfInspections;
    }

    @Override
    public void visitWorker(Worker worker) {

    }

    @Override
    public void visitRobot(Robot robot) {
        iLogger.info("Inspected " + robot + "; state: " + robot.getState().getClass().getSimpleName() + "; damage: " + robot.getDamage());
    }

    @Override
    public void visitMachine(Machine machine) {
        iLogger.info("Inspected " + machine + "; state: " + machine.getState().getClass().getSimpleName() + "; damage: " + machine.getDamage());
    }

    @Override
    public void visitItem(Item item) {

    }

}
