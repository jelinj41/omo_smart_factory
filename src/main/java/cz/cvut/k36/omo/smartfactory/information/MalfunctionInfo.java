package cz.cvut.k36.omo.smartfactory.information;

import cz.cvut.k36.omo.smartfactory.event.Event;
import cz.cvut.k36.omo.smartfactory.entities.Entity;

/**
 * Info about robot/machine malfunction
 */
public class MalfunctionInfo extends Event {

    private int priority;

    public MalfunctionInfo(Entity creator, int tick, int priority) {
        super(creator, tick);
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

}
