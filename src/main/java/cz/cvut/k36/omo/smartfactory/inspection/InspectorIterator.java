package cz.cvut.k36.omo.smartfactory.inspection;

import cz.cvut.k36.omo.smartfactory.production.Factory;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.entities.Entity;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;

import java.util.LinkedList;
import java.util.Queue;

/**
 * An iterator for director - inspection of all entities from most to least damaged
 */
public class InspectorIterator {

    private double lastDamage;
    private Factory factory;
    private Queue<Entity> sameDamageEntities;
    private boolean hasNext;

    public InspectorIterator(Factory factory) {
        lastDamage = 2;
        this.factory = factory;
        sameDamageEntities = new LinkedList<>();
        hasNext = true;
        double maxUnreturnedDamage = 0;

        for (ProductionLine productionLine : factory.getProductionLines()) {
            for (Entity entity : productionLine.getProductionEntities()) {
                if (entity instanceof Robot) {
                    double robotDamage = ((Robot) entity).getDamage();

                    if (robotDamage == maxUnreturnedDamage && robotDamage < lastDamage) {
                        sameDamageEntities.add(entity);
                    } else if (robotDamage > maxUnreturnedDamage && robotDamage < lastDamage) {
                        maxUnreturnedDamage = robotDamage;
                        sameDamageEntities.clear();
                        sameDamageEntities.add(entity);
                    }
                } else if (entity instanceof Machine) {
                    double machineDamage = ((Machine) entity).getDamage();

                    if (machineDamage == maxUnreturnedDamage && machineDamage < lastDamage) {
                        sameDamageEntities.add(entity);
                    } else if (machineDamage > maxUnreturnedDamage && machineDamage < lastDamage) {
                        maxUnreturnedDamage = machineDamage;
                        sameDamageEntities.clear();
                        sameDamageEntities.add(entity);
                    }
                }
            }
        }
        lastDamage = maxUnreturnedDamage;

        if (sameDamageEntities.isEmpty()) {
            hasNext = false;
        }
    }

    public boolean hasNext() {
        return hasNext;
    }

    public Entity nextEntity() {
        if (sameDamageEntities.size() > 1) {
            return sameDamageEntities.remove();

        } else if (sameDamageEntities.size() == 1) {
            Entity toReturn = sameDamageEntities.remove();

            double maxUnreturnedDamage = 0;

            for (ProductionLine productionLine : factory.getProductionLines()) {
                for (Entity entity : productionLine.getProductionEntities()) {
                    if (entity instanceof Robot) {
                        double robotDamage = ((Robot) entity).getDamage();

                        if (robotDamage == maxUnreturnedDamage && robotDamage < lastDamage) {
                            sameDamageEntities.add(entity);
                        } else if (robotDamage > maxUnreturnedDamage && robotDamage < lastDamage) {
                            maxUnreturnedDamage = robotDamage;
                            sameDamageEntities.clear();
                            sameDamageEntities.add(entity);
                        }

                    } else if (entity instanceof Machine) {
                        double machineDamage = ((Machine) entity).getDamage();

                        if (machineDamage == maxUnreturnedDamage && machineDamage < lastDamage) {
                            sameDamageEntities.add(entity);
                        } else if (machineDamage > maxUnreturnedDamage && machineDamage < lastDamage) {
                            maxUnreturnedDamage = machineDamage;
                            sameDamageEntities.clear();
                            sameDamageEntities.add(entity);
                        }
                    }
                }
            }
            lastDamage = maxUnreturnedDamage;

            if (sameDamageEntities.isEmpty()) {
                hasNext = false;
            }

            return toReturn;

        }

        return null;
    }
}
