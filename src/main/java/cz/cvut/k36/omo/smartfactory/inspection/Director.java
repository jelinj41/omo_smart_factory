package cz.cvut.k36.omo.smartfactory.inspection;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.entities.Worker;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;
import cz.cvut.k36.omo.smartfactory.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * The director inspects workers, robots, machines and items that are
 * currently on a production line. He "writes down" the state of all
 * entities using logger.
 */
public class Director implements InspectingPerson {

    private int numOfInspections = 1;
    public final Logger dLogger;
    private FileHandler fileHandler;

    public Director() {
        dLogger = Logger.getLogger(Director.class.getName());
        dLogger.setLevel(Level.INFO);
        makeNonExistingLogsDirectory();

        try {
            fileHandler = new FileHandler(Constants.LOGS_DIRECTORY_NAME + "/director.txt");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        fileHandler.setFormatter(new SimpleFormatter());
        dLogger.addHandler(fileHandler);
    }

    private void makeNonExistingLogsDirectory() {
        File logsDirectory = new File(Constants.LOGS_DIRECTORY_NAME);
        if (!logsDirectory.exists()) {
            boolean created = logsDirectory.mkdir();
            if (!created) {
                throw new RuntimeException("Logs directory is missing and was not created.");
            }
        }
    }

    public void addInspection() {
        numOfInspections++;
    }

    public int getNumOfInspections() {
        return numOfInspections;
    }

    @Override
    public void visitWorker(Worker worker) {
        dLogger.info("Inspected " + worker + "; state: " + worker.getState().getClass().getSimpleName());
    }

    @Override
    public void visitRobot(Robot robot) {
        dLogger.info("Inspected " + robot + "; state: " + robot.getState().getClass().getSimpleName());
    }

    @Override
    public void visitMachine(Machine machine) {
        dLogger.info("Inspected " + machine + "; state: " + machine.getState().getClass().getSimpleName());
    }

    @Override
    public void visitItem(Item item) {
        dLogger.info("Inspected " + item);
    }

}
