package cz.cvut.k36.omo.smartfactory.entities;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.entities.state.Free;
import cz.cvut.k36.omo.smartfactory.inspection.InspectingPerson;

/**
 * A machine that creates product items
 */
public class Machine extends Entity {

    private double damage = 0;
    private int fuel;
    private int electricity;

    public Machine(int cost, int fuel, int electricity) {
        super(cost, new Free());
        this.fuel = fuel;
        this.electricity = electricity;
    }

    @Override
    public void workOn(ProductionLine line, Item item) {
        getState().machineState(this, line, item);
    }

    @Override
    public void accept(InspectingPerson inspectingPerson) {
        inspectingPerson.visitMachine(this);
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getElectricity() {
        return electricity;
    }

    public void setElectricity(int electricity) {
        this.electricity = electricity;
    }

    @Override
    public String toString() {
        return "Machine(ID: " + getId() + ")";
    }

}
