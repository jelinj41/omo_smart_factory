package cz.cvut.k36.omo.smartfactory.production;

import cz.cvut.k36.omo.smartfactory.entities.Entity;
import cz.cvut.k36.omo.smartfactory.entities.Worker;
import cz.cvut.k36.omo.smartfactory.entities.Machine;
import cz.cvut.k36.omo.smartfactory.entities.Robot;
import cz.cvut.k36.omo.smartfactory.entities.state.Free;
import cz.cvut.k36.omo.smartfactory.entities.state.Working;
import cz.cvut.k36.omo.smartfactory.utils.RNG;

import java.util.*;

/**
 * Production line where items are manufactured
 */
public class ProductionLine {

    private int plID;
    private int priority;
    private List<Entity> config;
    private List<Entity> productionEntities;

    private Set<Material> materials;
    private int itemID;
    private List<Item> itemQueue;
    private int desiredItemsCount;
    private int producedItemsCount;

    private Product currProduct;
    private Map<Integer, Product> producedProducts;

    public ProductionLine(int plID, int priority) {
        this.plID = plID;
        this.priority = priority;
    }

    public int getPlID() {
        return plID;
    }

    public int getPriority() {
        return priority;
    }

    public List<Entity> getConfig() {
        return config;
    }

    public List<Entity> getProductionEntities() {
        return productionEntities;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public List<Item> getItemQueue() {
        return itemQueue;
    }

    public Product getCurrProduct() {
        return currProduct;
    }

    public Map<Integer, Product> getProducedProducts() {
        return producedProducts;
    }

    public boolean isFinished() {
        return producedItemsCount == desiredItemsCount;
    }

    public boolean isBroken() {
        return config.stream().anyMatch(Entity::isBroken);
    }

    public void createEntities(List<Product> products) {
        productionEntities = new ArrayList<>();
        Map<String, Integer> entitiesToCreate = new HashMap<>();
        entitiesToCreate.put("Worker", 0);
        entitiesToCreate.put("Machine", 0);
        entitiesToCreate.put("Robot", 0);

        for (Product product : products) {
            int workerC = 0;
            int machineC = 0;
            int robotC = 0;

            for (String className : product.getProductionLineConfig()) {
                if ("Worker".equals(className)) workerC++;
                if ("Machine".equals(className)) machineC++;
                if ("Robot".equals(className)) robotC++;
            }

            if (workerC > entitiesToCreate.get("Worker")) entitiesToCreate.put("Worker", workerC);
            if (machineC > entitiesToCreate.get("Machine")) entitiesToCreate.put("Machine", machineC);
            if (robotC > entitiesToCreate.get("Robot")) entitiesToCreate.put("Robot", robotC);
        }

        for (String className : entitiesToCreate.keySet()) {
            if ("Worker".equals(className)) {
                for (int i = 0; i < entitiesToCreate.get("Worker"); i++) {
                    productionEntities.add(new Worker(RNG.getInstanceOf().randomInt(10, 20)));
                }
            } else if ("Robot".equals(className)) {
                for (int i = 0; i < entitiesToCreate.get("Robot"); i++) {
                    productionEntities.add(new Robot(RNG.getInstanceOf().randomInt(10, 20), RNG.getInstanceOf().randomInt(15, 25), RNG.getInstanceOf().randomInt(15, 25)));
                }
            } else if ("Machine".equals(className)) {
                for (int i = 0; i < entitiesToCreate.get("Machine"); i++) {
                    productionEntities.add(new Machine(RNG.getInstanceOf().randomInt(10, 20), RNG.getInstanceOf().randomInt(15, 25), RNG.getInstanceOf().randomInt(10, 30)));
                }
            }
        }
    }

    public void configurePL(Product product, int itemCount) {
        productionEntities.forEach(e -> e.setOnPL(false));
        producedItemsCount = 0;
        itemID = 0;
        desiredItemsCount = itemCount;
        currProduct = product;

        if (producedProducts == null) {
            producedProducts = new HashMap<>();
        }

        producedProducts.put(Factory.getInstanceOf().getTick(), currProduct);
        config = new LinkedList<>();

        for (String className : product.getProductionLineConfig()) {
            Entity fe;

            if ("Worker".equals(className)) {
                fe = productionEntities.stream().filter(e -> e instanceof Worker && !e.isOnPL()).findFirst().get();
            } else if ("Robot".equals(className)) {
                fe = productionEntities.stream().filter(e -> e instanceof Robot && !e.isOnPL()).findFirst().get();
            } else if ("Machine".equals(className)) {
                fe = productionEntities.stream().filter(e -> e instanceof Machine && !e.isOnPL()).findFirst().get();
            } else {
                throw new RuntimeException("Unknown FactoryEntity type given for production line configuration!");
            }

            config.add(fe);
            fe.setOnPL(true);
        }

        materials = new HashSet<>(product.getMaterialsNeeded());
        itemQueue = new LinkedList<>(Collections.nCopies(config.size(), null));
    }

    public void run() {
        if (isFinished()) return;

        if (isBroken()) {
            System.out.println(this + " is broken and is waiting to be repaired.");
            return;
        }

        if (itemID + 1 <= desiredItemsCount) {
            itemQueue.add(0, new Item(plID + "-" + ++itemID, currProduct, new LinkedList<>(config)));
        } else if (itemQueue.size() + 1 <= config.size()) {
            itemQueue.add(0, null);
        }

        for (int i = 0; i < config.size(); i++) {
            Entity fe = config.get(i);
            Item item = itemQueue.get(i);

            if (item != null) {
                fe.setState(new Working());
            } else {
                fe.setState(new Free());
            }
            fe.workOn(this, item);
        }

        Item lastItem = itemQueue.get(itemQueue.size() - 1);

        if (lastItem != null) {
            producedItemsCount++;
            Factory.getInstanceOf().putItemToStorage(lastItem);
            System.out.println(lastItem + " finished!");
        }

        itemQueue.remove(itemQueue.size() - 1);
    }

    @Override
    public String toString() {
        return "ProductionLine (ID " + plID + ") for " + desiredItemsCount + "x " +
                currProduct + " with workers " +
                Arrays.toString(config.toArray());
    }

}
