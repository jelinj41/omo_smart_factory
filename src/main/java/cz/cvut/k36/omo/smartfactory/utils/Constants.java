package cz.cvut.k36.omo.smartfactory.utils;

/**
 * All constants in a program
 */
public abstract class Constants {

    public static double MACHINE_MATERIAL_CONSUMPTION = 1.4;
    public static double MACHINE_DAMAGE_ONETICK = 0.01;

    public static double ROBOT_MATERIAL_CONSUMPTION = 1.2;
    public static double ROBOT_DAMAGE_ONETICK = 0.015;

    public static double FUEL_COST = 2;
    public static double ELECTRICITY_COST = 1.5;

    public static String REPORTS_DIRECTORY_NAME = "reports";
    public static String LOGS_DIRECTORY_NAME = "logs";

}
