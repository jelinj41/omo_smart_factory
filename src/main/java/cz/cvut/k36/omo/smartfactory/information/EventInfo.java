package cz.cvut.k36.omo.smartfactory.information;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.event.Event;
import cz.cvut.k36.omo.smartfactory.entities.Entity;

/**
 * Info about an event during a single tick.
 */
public class EventInfo extends Event {

    private int cost;
    private int fuel;
    private int electricity;
    private Item item;

    public EventInfo(Entity creator, int tick, int cost, int fuel, int electricity, Item item) {
        super(creator, tick);
        this.cost = cost;
        this.fuel = fuel;
        this.electricity = electricity;
        this.item = item;
    }

    public int getCost() {
        return cost;
    }

    public int getFuel() {
        return fuel;
    }

    public int getElectricity() {
        return electricity;
    }

    public Item getItem() {
        return item;
    }
}
