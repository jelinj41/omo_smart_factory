package cz.cvut.k36.omo.smartfactory;

import cz.cvut.k36.omo.smartfactory.entities.Entity;
import cz.cvut.k36.omo.smartfactory.entities.state.State;
import cz.cvut.k36.omo.smartfactory.production.Factory;
import cz.cvut.k36.omo.smartfactory.reports.ConsumptionReportCreator;
import cz.cvut.k36.omo.smartfactory.reports.EventReportCreator;
import cz.cvut.k36.omo.smartfactory.reports.FactoryConfigurationReportCreator;
import cz.cvut.k36.omo.smartfactory.reports.OutagesReportCreator;

/**
 * Main function of the program
 */
public class Main {

    private static String configFileId;

    public static void main(String[] args) {
        try {
            configFileId = args[0];
        } catch (ArrayIndexOutOfBoundsException e) {
            configFileId = "1";
            System.out.println("Using config with id 1.");
        }

        Factory smartFactory = Factory.getInstanceOf();
        smartFactory.run();

        FactoryConfigurationReportCreator fcrc = new FactoryConfigurationReportCreator();
        fcrc.getReport(5).printToFile();

        ConsumptionReportCreator crc = new ConsumptionReportCreator();
        crc.getReport(5, 10).printToFile();

        EventReportCreator erc = new EventReportCreator();
        erc.getReport(5, 10).printToFile();

        OutagesReportCreator orc = new OutagesReportCreator();
        orc.getReport(0, 100).printToFile();

        Entity entity = smartFactory.getProductionLines().get(0).getProductionEntities().get(0);
        State state = entity.getStateInTick(300);
        System.out.println(entity.getClass().getSimpleName() + " " + entity.getId() + ", state " + state);
    }


    public static String getConfigFileId() {
        return configFileId;
    }


    public static void setConfigFileId(String configFileId) {
        Main.configFileId = configFileId;
    }
}
