package cz.cvut.k36.omo.smartfactory.event;

import cz.cvut.k36.omo.smartfactory.information.MalfunctionInfo;

import java.util.*;

/**
 * Object pool storing all events that have happened in the factory.
 */
public class EventPool implements Observer {

    private List<Event> eventList;
    private static EventPool instanceOf;

    private EventPool() {
        eventList = new LinkedList<>();
    }

    public List<Event> getEventList() {
        return Collections.unmodifiableList(eventList);
    }

    public static EventPool getInstanceOf() {
        if (instanceOf == null) {
            instanceOf = new EventPool();
        }

        return instanceOf;
    }

    @Override
    public void update(Event event) {
        eventList.add(event);
    }

    public static void clearEventPool() {
        instanceOf = null;
    }

    public Event getEvent(Listener listener, Class<? extends Event> eventClass) {
        Event foundEvent = null;

        if (eventClass.equals(MalfunctionInfo.class)) {
            MalfunctionInfo mEvent = getFirstMalfunctionEvent();
            if (mEvent != null) mEvent.setProcessedBy(listener);
            return mEvent;
        }

        for (Event event : eventList) {
            if (!event.isProcessed() && eventClass.isInstance(event)) {
                foundEvent = event;
                break;
            }
        }

        if (foundEvent != null) foundEvent.setProcessedBy(listener);

        return foundEvent;
    }

    private MalfunctionInfo getFirstMalfunctionEvent() {
        Optional<MalfunctionInfo> malEvent = eventList.stream()
                .filter(MalfunctionInfo.class::isInstance)
                .filter(e -> !e.isProcessed())
                .map(e -> (MalfunctionInfo) e)
                .max(Comparator.comparing(MalfunctionInfo::getPriority));
        return malEvent.orElse(null);
    }


    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("EventPool:\n");

        for (Event event : eventList) {
            str.append(event).append("\n");
        }

        return str.toString();
    }

}
