package cz.cvut.k36.omo.smartfactory.production;

import java.util.Objects;

/**
 * A material used to manufacture items
 */
public class Material {

    private String name;
    private int cost;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Material material = (Material) o;
        return getCost() == material.getCost() &&
                getName().equals(material.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCost());
    }

    @Override
    public String toString() {
        return "Material (" + name + ")";
    }
}
