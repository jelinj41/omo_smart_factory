package cz.cvut.k36.omo.smartfactory.utils;

import cz.cvut.k36.omo.smartfactory.configuration.FactoryConfig;

import java.util.Random;

/**
 * Random number generator
 */
public class RNG {

    private static RNG instanceOf = null;
    private long seed;
    private final Random random = new Random();

    private RNG() {
        setNewSeed(FactoryConfig.getInstanceOf().getRandomGeneratorSeed());
    }

    public static RNG getInstanceOf() {
        if (instanceOf == null) {
            instanceOf = new RNG();
        }
        return instanceOf;
    }

    public long getSeed() {
        return seed;
    }

    private void setNewSeed(String inputSeed) {
        if ("".equals(inputSeed)) {
            seed = random.nextLong();
        } else {
            try {
                seed = Long.parseLong(inputSeed);
            } catch (NumberFormatException ex) {
                //throw new InvalidConfigValueException("Incorrect value for seed: " + inputSeed);
                System.err.println("Incorrect value for seed: " + inputSeed);
                System.exit(1);
            }
        }

        random.setSeed(seed);
    }

    public double randomDouble(double min, double max) {
        return min + (max - min) * random.nextDouble();
    }

    public int randomInt(int min, int max) {
        return min + random.nextInt((max - min) + 1);
    }

    public boolean calculateProbability(double probability) {
        if (probability <= 0) {
            return false;
        } else if (probability >= 1) {
            return true;
        } else {
            return random.nextDouble() <= probability;
        }
    }

}
