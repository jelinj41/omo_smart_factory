package cz.cvut.k36.omo.smartfactory.entities;

import cz.cvut.k36.omo.smartfactory.production.Factory;
import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.event.*;
import cz.cvut.k36.omo.smartfactory.event.Observable;
import cz.cvut.k36.omo.smartfactory.event.Observer;
import cz.cvut.k36.omo.smartfactory.information.EventInfo;
import cz.cvut.k36.omo.smartfactory.information.MalfunctionInfo;
import cz.cvut.k36.omo.smartfactory.information.RepairEndedInfo;
import cz.cvut.k36.omo.smartfactory.entities.state.Broken;
import cz.cvut.k36.omo.smartfactory.entities.state.Free;
import cz.cvut.k36.omo.smartfactory.entities.state.State;
import cz.cvut.k36.omo.smartfactory.entities.state.Working;
import cz.cvut.k36.omo.smartfactory.inspection.Inspectable;
import cz.cvut.k36.omo.smartfactory.inspection.InspectingPerson;

import java.util.*;

/**
 * Abstract entity in the factory
 */
public abstract class Entity implements Observable, Inspectable {

    private int id;
    private State state;
    private int cost;
    private static int lastUsedId = 0;
    private boolean isOnPL;
    private Map<Integer, Event> events;
    private List<Observer> observers;

    public Entity(int cost, State state) {
        id = ++lastUsedId;
        this.state = state;
        this.cost = cost;
        isOnPL = false;
        events = new HashMap<>();
        observers = new ArrayList<>(Collections.singleton(EventPool.getInstanceOf()));
    }

    public abstract void workOn(ProductionLine line, Item item);

    @Override
    public abstract void accept(InspectingPerson inspectingPerson);

    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyAllObservers(Event event) {
        events.put(Factory.getInstanceOf().getTick(), event);
        observers.forEach((o) -> o.update(event));
    }

    public int getId() {
        return id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        if (!isBroken() || !(this instanceof Worker && state instanceof Broken)) {
            this.state = state;
        }
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isBroken() {
        return state instanceof Broken;
    }

    public boolean isWorking() {
        return state instanceof Working;
    }

    public boolean isFree() {
        return state instanceof Free;
    }

    public Map<Integer, Event> getEvents() {
        return events;
    }

    public boolean isOnPL() {
        return isOnPL;
    }

    public void setOnPL(boolean onPL) {
        this.isOnPL = onPL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return getId() == entity.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public State getStateInTick(int tick) {
        State state = new Free();
        for (int i = 0; i < tick; i++) {
            Event event = events.get(i);

            if (event instanceof EventInfo) {
                state = new Working();
            } else if (event instanceof MalfunctionInfo) {
                state = new Broken();
            } else if (event instanceof RepairEndedInfo) {
                state = new Free();
            }
        }
        return state;
    }

}
