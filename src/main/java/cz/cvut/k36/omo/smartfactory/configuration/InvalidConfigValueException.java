package cz.cvut.k36.omo.smartfactory.configuration;

/**
 * Indicates an invalid value in the factory configuration file.
 */
public class InvalidConfigValueException extends Exception {

    public InvalidConfigValueException(String message) {
        super(message);
    }

}
