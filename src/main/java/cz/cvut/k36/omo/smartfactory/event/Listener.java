package cz.cvut.k36.omo.smartfactory.event;

/**
 * A listener used for event processing.
 */
public interface Listener {

    public void listen();

}
