package cz.cvut.k36.omo.smartfactory.inspection;

/**
 * An interface with method to determine whether an item is inspectable
 */
public interface Inspectable {

    void accept(InspectingPerson inspectingPerson);

}
