package cz.cvut.k36.omo.smartfactory.entities;

import cz.cvut.k36.omo.smartfactory.production.Item;
import cz.cvut.k36.omo.smartfactory.production.ProductionLine;
import cz.cvut.k36.omo.smartfactory.entities.state.Free;
import cz.cvut.k36.omo.smartfactory.inspection.InspectingPerson;

/**
 * A worker who can both work on product items and be a repairman
 */
public class Worker extends Entity {

    public Worker(int cost) {
        super(cost, new Free());
    }

    @Override
    public void workOn(ProductionLine line, Item item) {
        getState().workerState(this, line, item);
    }

    @Override
    public void accept(InspectingPerson inspectingPerson) {
        inspectingPerson.visitWorker(this);
    }

    @Override
    public String toString() {
        return "Worker(ID: " + getId() + ")";
    }

}
